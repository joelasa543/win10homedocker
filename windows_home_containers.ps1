dism---deployment-image-servicing-and-management-technical-reference-for-windows
function Import-Feature {
    param( $Name )
    
    $packages = Get-ChildItem "C:\Windows\servicing\Packages\*$Name*.mum"
    foreach ($package in $packages) {
        Dism /online /NoRestart /Add-Package:"$package"
    }
    Dism /online /NoRestart /Enable-Feature /FeatureName:$Name -All /LimitAccess /ALL
}

Import-Feature -Name Microsoft-Hyper-V
Import-Feature -Name Containers 
Write-Output ""
Write-Output "redemarrer la machine et lancer windows_home_docker.ps1 pour installer dockre"