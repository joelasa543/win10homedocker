$windowsNT = 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion'
$EditionID = (Get-ItemProperty $windowsNT).EditionID

Set-ItemProperty -Path $windowsNT -Name 'EditionID' -Value 'Professional'
Write-Output ''
Write-Output '==========================================================================================='
Write-Output "EditionID temporairement modifié $EditionID to Professional                     "
Write-Output 'You lancer Docker Desktop Installer                                               '
Write-Output '                                                                                           '
Write-Output "confirmer pour restaurer $EditionID "
Write-Output '==========================================================================================='
Write-Output ''
Set-ItemProperty -Confirm -Path $windowsNT -Name 'EditionID' -value $EditionID
Write-Output ""
Write-Output "Bravo les gars!"